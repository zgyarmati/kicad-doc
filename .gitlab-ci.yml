# Default image and service for all jobs
image: docker:19.03.1
services:
  - docker:19.03.1-dind


stages:
  - build
  - review
  - push_container
  - deploy_container


# Build the doc container, provide it as artifact for the other jobs
# This job will push the artifact: "kicad-doc_{short commit sha}.tar.gz"
# This job will always run
build_container:
  stage: build
  script:
    - docker build -t "$CI_REGISTRY_IMAGE:latest" .
    - docker save "$CI_REGISTRY_IMAGE:latest" | gzip > kicad-doc_${CI_COMMIT_SHORT_SHA}.tar.gz
  artifacts:
    expire_in: 1 week
    paths:
      - kicad-doc_${CI_COMMIT_SHORT_SHA}.tar.gz
  only:
    - merge_requests  # Execute on merge request
    - branches
    - tags


# Copy the pdf and make them available for review
# This job will push the artifacts: "pdf review"
# This job will run only on merge request
review_doc:
  stage: review
  dependencies:
    - build_container
  before_script:
    - docker load < kicad-doc_${CI_COMMIT_SHORT_SHA}.tar.gz
  script:
    - export TEMP_CONTAINER=`docker create "$CI_REGISTRY_IMAGE:latest" /dev/null` # cerate a non running container from the image builded above
    - docker cp $TEMP_CONTAINER:/src kicad-doc                              # copy out the builded file from the container into kicad-doc (the folder is created by this command)
    - find kicad-doc -type f ! -iname "*.pdf" -delete                       # cleanup the artifact folder from non pdf files
    - find kicad-doc -type d -exec rmdir -p --ignore-fail-on-non-empty {} + # BusyBox "find" implementation lacks the -empty flag, this is a workaround for cleaning empty folder
  artifacts:
    name: "kicad-doc_${CI_COMMIT_SHORT_SHA}"
    expose_as: "pdf review"
    paths: ['kicad-doc/']
  only:
    - merge_requests  # Execute on merge request


# Push container in the docker registry with the master tag
# The container will be pushed with the tag: master
# This job should trigegr only on commit to master, and should ignore tagged commit
push_latest:
  stage: push_container
  dependencies:
    - build_container
  before_script:
    - docker info
    - docker login -u gitlab-ci-token -p "$CI_BUILD_TOKEN" "$CI_REGISTRY"
    - docker load < kicad-doc_${CI_COMMIT_SHORT_SHA}.tar.gz
  script:
    - docker tag "$CI_REGISTRY_IMAGE:latest" "$CI_REGISTRY_IMAGE:master"
    - docker push "$CI_REGISTRY_IMAGE:master"
  only:
    - master
  except:
    - merge_requests  # Do not execute on merge request
    - tags            # Do not execute on pushed tags


# Push container in the docker registry with the master and version(eg. 5.4) tag
# The container will be pushed with the tags: master, and the git tag (eg. 5.4)
# This build should run only on pushed tags
push_tag:
  stage: push_container
  dependencies:
    - build_container
  before_script:
    - docker info
    - docker login -u gitlab-ci-token -p "$CI_BUILD_TOKEN" "$CI_REGISTRY"
    - docker load < kicad-doc_${CI_COMMIT_SHORT_SHA}.tar.gz
  script:
    - docker tag "$CI_REGISTRY_IMAGE:latest" "$CI_REGISTRY_IMAGE:${CI_COMMIT_TAG}"
    - docker push "$CI_REGISTRY_IMAGE:${CI_COMMIT_TAG}"
  only:
    - tags            # Execute only on pushed tags
  except:
    - merge_requests  # Do not execute on merge request


# Deploy on the web the latest container
# This will trigger the pipeline in the "kicad/services/kicad-doc-website"
# This job will run only on the master branch of the main project(KiCad/KiCad Web Services/kicad-doc)
deploy_doc_on the_www:
  stage: deploy_container
  trigger:
    project: kicad/services/kicad-doc-website
    branch: master
  only:
    - master@kicad/services/kicad-doc # Execute only on master commit in the main repo
  except:
    - merge_requests  # Do not execute on merge request
